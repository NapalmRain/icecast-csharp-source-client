﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Documents;

namespace IceCastLibrary {
	public delegate void SongStoped( );
	/// <summary>
	/// Сласс для работы с сервером IceCast
	/// </summary>
	public class IceCastClient {
		public event SongStoped isSongStoped;
		public event SongStoped isPlaylistEnded;

		private Libshout icecast;
		private byte[] buff = new byte[ 4096 ];
		private int read;
		private string lastError;
		private BackgroundWorker bg;
		private bool isPlaying = false;
		private int currentSong = 0;
		private List<string> playList;


		private class WorkerArguments {
			public string fileName;
		}

		/// <summary>
		/// Установить параметр описания
		/// </summary>
		/// <param name="description">Текст описания радиостанции</param>
		public void SetDescription(string description) {
			icecast.setDescription( description );
		}

		/// <summary>
		/// Установка порта соединения
		/// </summary>
		/// <param name="port"></param>
		public void SetPort(int port) {
			icecast.setPort(port);
		}


		/// <summary>
		/// Получить жанр
		/// </summary>
		/// <returns></returns>
		public string GetGenre() {
			return icecast.getGenre();
		}

		/// <summary>
		/// Установка жанра
		/// </summary>
		/// <param name="ganre"></param>
		public void SetGanre(string ganre ) {
			icecast.setGenre(ganre);
		}

		/// <summary>
		/// Создаём экземпляр клиента
		/// </summary>
		/// <param name="host">адрес сервера</param>
		/// <param name="port">порт сервера</param>
		/// <param name="pass">пароль для доступа</param>
		/// <param name="stationName">наименование станции</param>
		/// <param name="Format">Формат файлов для стриминга. По умолчанию OGG</param>
		public IceCastClient( string host, int port, string pass, string stationName, FormatTypes Format = FormatTypes.OGG ) {
			UnloadDlls();

			playList = new List<string>();

			icecast = new Libshout();
			icecast.setProtocol( 0 );
			icecast.setHost( host );
			icecast.setPort( port );
			icecast.setPassword( pass );
			icecast.setFormat( (int)Format );
			icecast.setPublic( true );
			icecast.setName( stationName );
			icecast.setMount( "/live" );

			bg = new BackgroundWorker();
			bg.DoWork += RunSendingForm;
			bg.RunWorkerCompleted += isCompleted;
			bg.WorkerSupportsCancellation = true;
		}

		/// <summary>
		/// Передача библиотеке списка песен для воспроизведения
		/// </summary>
		/// <param name="files"></param>
		public void SetPlaylist(List<string> files) {
			playList = files;
		}

		/// <summary>
		/// Add song to playlist
		/// </summary>
		/// <param name="fileName"></param>
		public void AddSong(string fileName) {
			playList.Add(fileName);
		}

		/// <summary>
		/// Playing song from playlist
		/// </summary>
		/// <param name="index"></param>
		public void PlaySong(int index) {
			if (playList.Count <= index) {
				throw new Exception("Song numder out from playlist. Playlist count is "+ playList.Count+", indes is "+ index );
			}
			currentSong = index;
			PlaySong( playList[ index ] );
		}

		/// <summary>
		/// Получить имя текущей песни
		/// </summary>
		/// <returns></returns>
		public string GetCurrentSongName() {
			return playList[ currentSong ];
		}

		/// <summary>
		/// Start playing next song from playlist
		/// </summary>
		public int PlayNext() {
			currentSong++;
			
			if (currentSong >= playList.Count) {
				isPlaylistEnded?.Invoke();
				return -1;
			}

			PlaySong( currentSong );
			return currentSong;
		}

		/// <summary>
		/// Start playing previous song from playlist
		/// </summary>
		public void PlayPrevious() {
			currentSong--;

			if ( currentSong < 0 ) {
				isPlaylistEnded?.Invoke();
				return;
			}
			
			PlaySong( currentSong );
		}

		/// <summary>
		/// Если вы хотите, чтобы адрес вашей трансляции отличался от /live
		/// </summary>
		/// <param name="mount"></param>
		public void SetMount( string mount ) {
			icecast.setMount( mount );
		}

		/// <summary>
		/// Установка нового формата стриминга
		/// </summary>
		/// <param name="Format"></param>
		public void SetFormat( FormatTypes Format ) {
			icecast.setFormat( (int)Format );
		}

		private void UnloadDlls() {
			var assembly = Assembly.GetExecutingAssembly();
			string[] names = assembly.GetManifestResourceNames();


			foreach ( string library in names ) {
				string fileName = library.Substring( library.IndexOf( '.' ) + 1 );
				fileName = fileName.Substring( fileName.IndexOf( '.' ) + 1 );
				if ( !File.Exists( fileName ) ) {
					using ( var stream = assembly.GetManifestResourceStream( library ) ) {
						using ( var rer = new BinaryReader( stream ) ) {
							using ( FileStream streamOut = new FileStream( fileName, FileMode.Create ) ) {
								using ( BinaryWriter wrer = new BinaryWriter( streamOut ) ) {
									wrer.Write( rer.ReadBytes( (int)stream.Length ) );
								}
							}
						}
					}
				}
			}
		}

		/// <summary>
		/// Попытка открыть подключение
		/// </summary>
		/// <returns></returns>
		public bool Open() {
			try {
				icecast.open();
				return icecast.isConnected();
			} catch ( Exception e ) {
				lastError = e.Message;
				return false;
			}
		}

		/// <summary>
		/// Получение текста последней ошибки
		/// </summary>
		/// <returns></returns>

		public string GetLastError() {
			return lastError;
		}

		/// <summary>
		/// Получение текста ошибки от класса более низкого уровня
		/// </summary>
		/// <returns></returns>
		public string GetIceCastError() {
			return icecast.GetError();
		}

		public void Stop() {
			if ( bg.IsBusy ) {
				bg.CancelAsync();
			}
			if (isPlaying) {
				isPlaying = false;
			}
		}

		/// <summary>
		/// Начало проигрывания файла
		/// </summary>
		/// <param name="fileName">путь до файла, который необходимо отправить на сервер</param>
		public void PlaySong( string fileName ) {
			Stop();
			string path = Path.GetFullPath( fileName );
			if (!File.Exists( path ) ) {
				throw new Exception("Song not found in Path: "+path);
			}
			
			WorkerArguments args = new WorkerArguments { fileName = fileName };
			bg.RunWorkerAsync( args );
		}

		private void isCompleted( object sender, RunWorkerCompletedEventArgs e ) {
			isPlaying = false;
			isSongStoped?.Invoke();
		}

		private void RunSendingForm( object sender, DoWorkEventArgs e ) {
			isPlaying = true;
			WorkerArguments args = (WorkerArguments)e.Argument;
			Stream file = File.Open( args.fileName, FileMode.Open );
			BinaryReader reader = new BinaryReader( file );
			int total = 0;
			while ( true ) {
				read = reader.Read( buff, 0, buff.Length );
				total = total + read;
				if ( read > 0 ) {
					icecast.send( buff, read );
				} else break;
			}
			reader.Close();
			reader.Dispose();
			file.Close();
			file.Dispose();
		}

		/// <summary>
		/// Закрыть соединение
		/// </summary>
		public void Close() {
			icecast.close();
		}
	}
}
